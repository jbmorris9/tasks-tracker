import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
  //component input

  constructor() {
  }

  ngOnInit(): void {
  }

  //build-in to input dynamic text
  @Input() color!:string;
  @Input() text!: string;

  //build-in 
  @Output() btnClick = new EventEmitter();

  //onclick method/event
  onClick(){
    this.btnClick.emit();
  }
}
