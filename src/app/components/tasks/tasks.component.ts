import { Component, OnInit } from '@angular/core';
import { Task } from '../../task';
import { TaskService } from '../../services/task.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  constructor(private taskServive: TaskService) { }

  ngOnInit(): void {
    this.taskServive.getTasks().subscribe((tasks) => (this.tasks = this.tasks = tasks));
  }

  tasks: Task[] = [];

  deleteTask(task: Task){
    
    this.taskServive
      .deleteTask(task)
      .subscribe(
        () => (this.tasks = this.tasks.filter((t) => t.id ! == task.id))
      );
  }
}
