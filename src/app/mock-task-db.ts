import { Task } from "./task";

export const TASKS: Task[] = [
    {
        id: 1,
        text: "Clean apartment",
        date: "07/15/2021",
        reminder: false
    },
    {
        id: 2,
        text: "Go to dentist appointment",
        date: "07/19/2021",
        reminder: true
    },
    {
        id: 3,
        text: "Laundry",
        date: "07/20/2021",
        reminder: true
    },
    {
        id: 4,
        text: "Wash dishes",
        date: "07/21/2021",
        reminder: true
    },
    {
        id: 5,
        text: "Pick package - parents' house",
        date: "07/21/2021",
        reminder: true
    }
];