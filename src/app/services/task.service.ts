import { Injectable } from '@angular/core';
import { Observable, /* of */ } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http'
//import { TASKS } from '../mock-task-db';
import { Task } from '../task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private apiUrl = 'http://localhost:3000/tasks';

  constructor(private http: HttpClient) { }

  getTasks(): Observable<Task[]> {

    //const tasks = of(TASKS);
    //return TASKS;
    //return tasks;

    return this.http.get<Task[]>(this.apiUrl);
  }

  deleteTask(task: Task): Observable<Task> {

    console.log('Bdfdfd');
    

    const url = `${this.apiUrl}/${task.id}`;

    return this.http.delete<Task>(url);
  }
}
